@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    You are logged in!

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('items.list') }}">{{ __('商品一覧') }}</a>
                        <a class="nav-link" href="{{ route('itmes.create') }}">{{ __('商品新規登録') }}</a>
                        <a class="nav-link" href="{{ route('inquiry') }}">{{ __('お問い合わせ') }}</a>
                    </li>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection