<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/items_list', 'HomeController@get')->name('items.list');

Route::get('/items_create_page', 'HomeController@create')->name('itmes.create');

Route::get('/inquiry', 'HomeController@inquiry')->name('inquiry');
